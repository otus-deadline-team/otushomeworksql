﻿namespace AvitoAPI.Models
{
    public class SaleItem
    {
        public int Id { get; set; } 
        public int UserId { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
    }
}
