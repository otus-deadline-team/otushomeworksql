﻿namespace AvitoAPI.Models
{
    /// <summary>
    /// Модель используемая для создания пользователя
    /// </summary>
    public class UserCreateDto
    {
        public string? FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string? LastName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string? Patronymic { get; set; }
        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; set; }
    }
}
