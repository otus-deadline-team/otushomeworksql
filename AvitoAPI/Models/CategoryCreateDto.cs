﻿namespace AvitoAPI.Models
{
    /// <summary>
    /// Категории
    /// </summary>
    public class CategoryCreateDto
    {
        /// <summary>
        /// Название категории
        /// </summary>
        public string Name { get; set; }
    }
}
