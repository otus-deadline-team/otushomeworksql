﻿using AvitoAPI.Models;
using AvitoAPI.Services.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace AvitoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategorysController : Controller
    {
        private readonly ILogger<UsersController> _logger;
        private readonly ICategoryService _categoryService;

        public CategorysController(ILogger<UsersController> logger, ICategoryService categoryService)
        {
            _logger = logger;
            _categoryService = categoryService;
        }

        /// <summary>
        /// Возвращает всех пользователей
        /// </summary>
        /// <response code="200">Если запрос выполнен успешно</response>
        /// <response code="500">Если во время выполнения произошла ошибка</response>
        [Route("all")]
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_categoryService.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <response code="200">Если запрос выполнен успешно</response>
        /// <response code="500">Если во время выполнения произошла ошибка</response>
        [Route("create")]
        [HttpPost]
        public IActionResult CreateCategory([FromForm] CategoryCreateDto category)
        {
            try
            {
                return Ok(_categoryService.CreateCategory(category));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}