using AvitoAPI.Models;
using AvitoAPI.Services.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace AvitoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IUserService _userService;

        public UsersController(ILogger<UsersController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        /// <summary>
        /// ���������� ���� �������������
        /// </summary>
        /// <response code="200">���� ������ �������� �������</response>
        /// <response code="500">���� �� ����� ���������� ��������� ������</response>
        [Route("all")]
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_userService.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        /// <summary>
        /// ������� ������������
        /// </summary>
        /// <response code="200">���� ������ �������� �������</response>
        /// <response code="500">���� �� ����� ���������� ��������� ������</response>
        [Route("create")]
        [HttpPost]
        public IActionResult CreateUser([FromForm] UserCreateDto user)
        {
            try
            {
                return Ok(_userService.CreateUser(user));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}