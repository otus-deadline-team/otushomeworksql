﻿using AvitoAPI.Models;
using AvitoAPI.Services.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace AvitoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaleItemsController : Controller
    {
        private readonly ILogger<SaleItemsController> _logger;
        private readonly ISaleItemService _saleItemsService;
        private readonly IUserService _userService;
        private readonly ICategoryService _categoryService;

        public SaleItemsController(ILogger<SaleItemsController> logger, ISaleItemService saleItemService, IUserService userService, ICategoryService categoryService)
        {
            _logger = logger;
            _saleItemsService = saleItemService;
            _userService = userService;
            _categoryService = categoryService;
        }

        /// <summary>
        /// Возвращает весь товар
        /// </summary>
        /// <response code="200">Если запрос выполнен успешно</response>
        /// <response code="500">Если во время выполнения произошла ошибка</response>
        [Route("all")]
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_saleItemsService.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        /// <summary>
        /// Создать товар пользователю
        /// </summary>
        /// <response code="200">Если запрос выполнен успешно</response>
        /// <response code="500">Если во время выполнения произошла ошибка</response>
        [Route("create")]
        [HttpPost]
        public IActionResult CreateSaleItem([FromForm] SaleItemCreateDto saleItem)
        {
            try
            {
                if (!(_userService.GetAll().Any(x => x.Id == saleItem.UserId)))
                {
                    return BadRequest($"Пользователя с Id = {saleItem.UserId} не существует.");
                }
                else if (!(_categoryService.GetAll().Any(x => x.Id == saleItem.CategoryId)))
                {
                    return BadRequest($"Категории товара с Id = {saleItem.CategoryId} не существует.");
                }
                else
                {
                    return Ok(_saleItemsService.CreateSaleItem(saleItem));
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}