﻿using AvitoAPI.Models;
using AvitoAPI.Repositories.Abstract;
using Npgsql;
using Dapper;

namespace AvitoAPI.Repositories.Implementations
{
    public class SaleItemRepository : ISaleItemRepository
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly string _connectionString;
        public SaleItemRepository(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _connectionString = configuration.GetConnectionString("AvitoDB");
            _loggerFactory = loggerFactory;
        }

        public List<SaleItem> GetAll()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString: _connectionString);
            connection.Open();
            List<SaleItem> result = connection.Query<SaleItem>(GetSaleItemsQuery).ToList();
            connection.Close();
            return result;
        }

        public int CreateSaleItem(SaleItemCreateDto saleItem)
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString: _connectionString);
            connection.Open();
            using NpgsqlCommand cmd = new NpgsqlCommand(cmdText: CreateSaleItemCommand, connection: connection);
            NpgsqlParameterCollection parameters = cmd.Parameters;
            parameters.Add(value: new NpgsqlParameter(parameterName: "@userId", value: saleItem.UserId));
            parameters.Add(value: new NpgsqlParameter(parameterName: "@name", value: saleItem.Name));
            parameters.Add(value: new NpgsqlParameter(parameterName: "@description", value: saleItem.Description));
            parameters.Add(value: new NpgsqlParameter(parameterName: "@price", value: saleItem.Price));
            parameters.Add(value: new NpgsqlParameter(parameterName: "@categoryId", value: saleItem.CategoryId));
            return (int)(long)cmd.ExecuteScalar();
        }

        #region SqlQueries
        private const string GetSaleItemsQuery = @"
            SELECT
                Id,
                UserId,
                Name,
                Description,
                Price,
                CategoryId
            FROM
                SaleItems
        ";
        private const string CreateSaleItemCommand = @"
            INSERT INTO SaleItems(UserId,
                Name,
                Description,
                Price,
                CategoryId)
	            VALUES (@userId, @name, @description, @price, @categoryId)
            RETURNING id;
        ";
        #endregion
    }
}
