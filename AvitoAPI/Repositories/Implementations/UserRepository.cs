﻿using AvitoAPI.Models;
using AvitoAPI.Repositories.Abstract;
using Npgsql;
using Dapper;

namespace AvitoAPI.Repositories.Implementations
{
    public class UserRepository : IUserRepository
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly string _connectionString;
        public UserRepository(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _connectionString = configuration.GetConnectionString("AvitoDB");
            _loggerFactory = loggerFactory;
        }

        public List<User> GetAll()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString: _connectionString);
            connection.Open();
            List<User> result = connection.Query<User>(GetUsersQuery).ToList();
            connection.Close();
            return result;
        }

        public int CreateUser(UserCreateDto user)
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString: _connectionString);
            connection.Open();
            using NpgsqlCommand cmd = new NpgsqlCommand(cmdText: CreateUserCommand, connection: connection);
            NpgsqlParameterCollection parameters = cmd.Parameters;
            parameters.Add(value: new NpgsqlParameter(parameterName: "@firstname", value: user.FirstName ?? String.Empty));
            parameters.Add(value: new NpgsqlParameter(parameterName: "@lastname", value: user?.LastName ?? String.Empty));
            parameters.Add(value: new NpgsqlParameter(parameterName: "@patronymic", value: user?.Patronymic ?? String.Empty));
            parameters.Add(value: new NpgsqlParameter(parameterName: "@email", value: user.Email.ToLower()));
            return (int)(long)cmd.ExecuteScalar();
        }

        #region SqlQueries
        private const string GetUsersQuery = @"
            SELECT
                Id,
                FirstName,
                LastName,
                Patronymic,
                Email
            FROM
                Users
        ";
        private const string CreateUserCommand = @"
            INSERT INTO users(
	            firstname, lastname, patronymic, email)
	            VALUES (@firstname, @lastname, @patronymic, @email)
            RETURNING id;
        ";
        #endregion
    }
}
