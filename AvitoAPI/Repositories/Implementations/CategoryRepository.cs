﻿using AvitoAPI.Models;
using AvitoAPI.Repositories.Abstract;
using Npgsql;
using Dapper;


namespace AvitoAPI.Repositories.Implementations
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly string _connectionString;
        public CategoryRepository(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _connectionString = configuration.GetConnectionString("AvitoDB");
            _loggerFactory = loggerFactory;
        }

        public List<Category> GetAll()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString: _connectionString);
            connection.Open();
            List<Category> result = connection.Query<Category>(GetCategorysQuery).ToList();
            connection.Close();
            return result;
        }

        public int CreateCategory(CategoryCreateDto category)
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString: _connectionString);
            connection.Open();
            using NpgsqlCommand cmd = new NpgsqlCommand(cmdText: CreateCategoryCommand, connection: connection);
            NpgsqlParameterCollection parameters = cmd.Parameters;
            parameters.Add(value: new NpgsqlParameter(parameterName: "@name", value: category.Name));
            return (int)(long)cmd.ExecuteScalar();
        }

        #region SqlQueries
        private const string GetCategorysQuery = @"
            SELECT
                Id,
                Name
            FROM
                Categorys
        ";
        private const string CreateCategoryCommand = @"
            INSERT INTO Categorys(Name)
	            VALUES (@name)
            RETURNING id;
        ";
        #endregion
    }
}
