﻿using AvitoAPI.Models;

namespace AvitoAPI.Repositories.Abstract
{
    public interface ICategoryRepository
    {
        /// <summary>
        /// Возвращает все категории
        /// </summary>
        /// <returns></returns>
        List<Category> GetAll();
        /// <summary>
        /// Добавить категорию 
        /// </summary>
        /// <param name="category">Категория</param>
        /// <returns></returns>
        int CreateCategory(CategoryCreateDto category);
    }
}
