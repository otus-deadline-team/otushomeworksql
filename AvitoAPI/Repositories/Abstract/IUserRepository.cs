﻿using AvitoAPI.Models;

namespace AvitoAPI.Repositories.Abstract
{
    public interface IUserRepository
    {
        /// <summary>
        /// Возвращает всех пользователей
        /// </summary>
        /// <returns></returns>
        List<User> GetAll();
        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns></returns>
        int CreateUser(UserCreateDto user);
    }
}
