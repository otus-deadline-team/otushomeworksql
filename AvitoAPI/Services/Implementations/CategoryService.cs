﻿using AvitoAPI.Models;
using AvitoAPI.Repositories.Abstract;
using AvitoAPI.Services.Abstract;

namespace AvitoAPI.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public List<Category> GetAll()
        {
            return _categoryRepository.GetAll();
        }
        public int CreateCategory(CategoryCreateDto category)
        {
            if (!string.IsNullOrEmpty(category?.Name))
            {
                return _categoryRepository.CreateCategory(category);
            }
            else
            {
                throw new Exception("Имя категории должно быть указано");
            }
        }
    }
}
