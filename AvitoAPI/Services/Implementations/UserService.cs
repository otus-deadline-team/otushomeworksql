﻿using AvitoAPI.Models;
using AvitoAPI.Repositories.Abstract;
using AvitoAPI.Services.Abstract;

namespace AvitoAPI.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public List<User> GetAll()
        {
            return _userRepository.GetAll();
        }
        public int CreateUser(UserCreateDto user)
        {
            if (!string.IsNullOrEmpty(user?.Email))
            {
                return _userRepository.CreateUser(user);
            }
            else
            {
                throw new Exception("Не указана почта пользователя");
            }
        }
    }
}
