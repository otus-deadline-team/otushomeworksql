﻿using AvitoAPI.Models;
using AvitoAPI.Repositories.Abstract;
using AvitoAPI.Services.Abstract;

namespace AvitoAPI.Services.Implementations
{
    public class SaleItemService : ISaleItemService
    {
        private readonly ISaleItemRepository _saleItemRepository;

        public SaleItemService(ISaleItemRepository saleItemRepository)
        {
            _saleItemRepository = saleItemRepository;
        }
        public List<SaleItem> GetAll()
        {
            return _saleItemRepository.GetAll();
        }
        public int CreateSaleItem(SaleItemCreateDto saleItem)
        {
            if (!string.IsNullOrEmpty(saleItem?.Name))
            {
                return _saleItemRepository.CreateSaleItem(saleItem);
            }
            else
            {
                throw new Exception("Имя товара должно быть указано");
            }
        }
    }
}
