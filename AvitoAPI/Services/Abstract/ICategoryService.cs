﻿using AvitoAPI.Models;

namespace AvitoAPI.Services.Abstract
{
    public interface ICategoryService
    {
        /// <summary>
        /// Возвращает все категории
        /// </summary>
        /// <returns></returns>
        List<Category> GetAll();
        /// <summary>
        /// Добавить категорию 
        /// </summary>
        /// <param name="category">Категория</param>
        /// <returns></returns>
        int CreateCategory(CategoryCreateDto category);
    }
}
