﻿using AvitoAPI.Models;

namespace AvitoAPI.Services.Abstract
{
    public interface ISaleItemService
    {
        /// <summary>
        /// Возвращает все товары
        /// </summary>
        /// <returns></returns>
        List<SaleItem> GetAll();
        /// <summary>
        /// Добавить товар 
        /// </summary>
        /// <param name="saleItem">создаваемый товар</param>
        /// <returns></returns>
        int CreateSaleItem(SaleItemCreateDto saleItem);
    }
}
