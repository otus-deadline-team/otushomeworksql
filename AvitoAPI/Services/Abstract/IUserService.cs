﻿using AvitoAPI.Models;

namespace AvitoAPI.Services.Abstract
{
    public interface IUserService
    {
        /// <summary>
        /// Возвращает всех пользователей
        /// </summary>
        /// <returns></returns>
        List<User> GetAll();
        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns></returns>
        int CreateUser(UserCreateDto user);
    }
}
